# scanning for message content
matches = re.finditer(msg_keyword, logcontent, re.IGNORECASE)
for match in matches:
    msg_keyword_match = match.group(1)
    logging.error(f"MSG_KEYWORD_MATCH>>> {msg_keyword_match}")
    break

logging.error(f"keyword len {len(msg_keyword_match.split(','))}")
if len(msg_keyword_match) and len(msg_keyword_match.split(",")) > msg_keyword_position:
    if "http" in content: # because /2 needs to be moder than floor() = 1
        content=(msg_keyword_match.split(",")[msg_keyword_position]).strip().rsplit(' ',1)[0]
    else:
        content=(msg_keyword_match.split(",")[msg_keyword_position]).strip()
    logging.error(f"content to correlate>> {content}")
    if len(content)>=3 and content !="undefined" and content !="null" :
        if "http" in content: # because /2 needs to be moder than floor() = 1
            content=content[0:math.floor(len(content)/2)-1]
        correlated_results=correl_string(correlated_results,my_app_name,content,correlate_other_jitmf_events=True)

# Example message content keyword:
# >>>> Message_Keyword_Regex_Match >>> (1109846348587321怀觧v,0,獭彧摩,0,DHL: Your parcel is arriving, track here: https://flexis搯瑡⽡獵牥〯振浯攮慸灭敬琮畲瑳摥潣瑮楡敮癲物畴污灡⽰楶瑲慵⽬慤慴甯敳⽲⼰潣⹭浩⹯湡牤楯⹤浩楯⽭楦敬⽳汸杯/,0,1677514271361900000,0,{"msg_id":"7VAdtUjX"},1,Patrick￿敭獳条獥戮極d＀,7VAdtUjX敭獳条獥戮極d＀,1677514271361900000)|Message Sent
# >>>> Content to correlate >>> DHL: Your parcel is arriving

# scanning for message id
logging.error(f">>>>>MSG_KEYWORKD>> {msg_keyword} >>>>LOG CONTENT>> {logcontent} >>>>>CONTENT>> {content} >>>>>mid regex>> {mid_regex}")
matches = re.finditer(mid_regex, logcontent, re.IGNORECASE)
for match in matches:
    mid_keyword_match = match.group(1)
    content=mid_keyword_match
    break

if len(content) and content !="undefined" and content !="null":
    correlated_results=correl_string(correlated_results,my_app_name,content, delete_only=True)