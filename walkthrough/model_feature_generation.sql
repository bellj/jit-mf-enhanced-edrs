-- Table generation based on features selection
CREATE TABLE IF NOT EXISTS `utility-ratio-120412.grr_app_models.whatsapp_table_60s` AS
(
  SELECT event_date, COUNT(*) as total_events, COUNTIF(source='AutoExportedAndroidCallLog') AS autoexportedandroidcalllog_data, COUNTIF(source='AutoExportedAndroidSmsMms') AS autoexportedandroidsmsmms_data, COUNTIF(source='AutoExportedAndroidSensorInfo') AS autoexportedandroidsensorinfo_data, COUNTIF(source='messenger_data') AS messenger_data, COUNTIF(source='AutoExportedAndroidPackageInfo') AS package_info_data, COUNTIF(source='new_jitmf_data') AS jitmf_data, COUNTIF(CONTAINS_SUBSTR(event_details, "DELETE")) as delete_number, COUNTIF(CONTAINS_SUBSTR(event_details, "INSERT")) as insert_number, COUNTIF(CONTAINS_SUBSTR(event_details, "REPLACE")) as replace_number, COUNTIF(CONTAINS_SUBSTR(event_details, "UPDATE")) as update_number, COUNTIF(CONTAINS_SUBSTR(event_details, "SELECT")) as select_number
  FROM(
    SELECT * FROM 
    (
      SELECT TIMESTAMP_SECONDS(60 * DIV(unix_seconds(event_date), 60)) as event_date, event_details, source
      from (
          SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidCallLog` 
          UNION ALL
          SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidPackageInfo` 
          UNION ALL
          SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSensorInfo` 
          UNION ALL
          SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSmsMms` 
          UNION ALL
          SELECT * from (
              select * from (
                  SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`
          )
          WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%' or upper(event_details) like 'SELECT%'  )
          AND (lower(event_details) like '% {app_table_name.lower} %' OR lower(event_details) like '% {app_table_name}(%')    
          )
          UNION ALL
          {app_table_query}
          UNION ALL
          SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_whatsapp.all_telegram_data`
      )
        WHERE event_date IS NOT NULL 
        AND event_date >= "{app_start_date}"
        AND event_date <= "{app_end_date}"
    )
  )
  GROUP BY event_date
);