# Walkthrough contents

![Attack Detection Workflow](attack_detection_workflow.png)
`investigation_script.py` - \textit{Attack Detection} component script. This script comprises the snippets explained below and their execution flow, to produce a timeline of attack steps for the case studies executed.

Step 1 Material:
`grr-server.yaml` - ReLF config file, updated to include Google BigQuery json service account.

Step 3 Material:
`whatsapp_config.json` - App-specific config for WhatsApp.

Step 5 Material:
`model_feature_generation.sql` - K-means, PCA and Autoencoder features generated for each case study and model.
`model_generation.sql` - PCA model generation query used to generate PCA model based on the features generated in `model_feature_generation.sql`, with a time grouping of 60s, for the WhatsApp case study.

Step 6 Material:
`model_execution.sql` - Executes the model generated, based on a set threshold, model and feature table (in the case of K-Means, PCA and Autoencoder), to obtain timeslices that are detected as those during which anomalous events occur.

Step 7 Material:
`get_jitmf_logs.sql` - Gets all JIT-MF logs in a time period. The time period is defined as that time which the model has detected as during which an anomalous event occurred.

Step 8 Material:
`keyword_identification.py` - Scans each anomalous JIT-MF log entry for message content and message ID keyword, based on app-specific regex. This keyword is then used for correlation.

Step 9 Material:
`correlate_string.py` - This is the function used to correlate events from different sources based on the selected keywords.