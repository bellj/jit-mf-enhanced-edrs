select * from (
  SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`
)
WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'REPLACE%'  )
AND (lower(event_details) like '% {app_table_name.lower()} %' OR lower(event_details) like '% {app_table_name.lower()}(%')    
AND (  TIMESTAMP_SECONDS(time) >= '{anomaly_time.strftime("%Y-%m-%d %H:%M:00")}') 
AND (  TIMESTAMP_SECONDS(time) <= '{anomaly_time.strftime("%Y-%m-%d %H:%M:59")}') 
order by TIMESTAMP_SECONDS(time) asc