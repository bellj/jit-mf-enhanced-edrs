def correl_string(result_set,my_app_name,keyword_content, correlate_other_jitmf_events=False, delete_only=False):
    app_table_query=apps[my_app_name]["table_query"]
    app_start_date=apps[my_app_name]["start_date"]
    app_end_date=apps[my_app_name]["end_date"]

    app_name=my_app_name
    if my_app_name =="telegram":
        all_telegram_tbl_union=""
        app_name="2"
    else:
        all_telegram_tbl_union=f"""UNION ALL
                    SELECT PARSE_TIMESTAMP("%d.%m.%Y %H:%M:%S",date,"CET") AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_{app_name}.all_telegram_data`"""
    app_table_name=apps[my_app_name]["table_name"]
    msg_keyword=apps[my_app_name]["msg_keyword_regex"]
    msg_keyword_position=apps[my_app_name]["msg_keyword_position"]
    keyword_content = keyword_content.replace('href=\\','href') # MANUALLY ESCAPING THIS
    query = f"""select * from (select  *
        from (
            SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidCallLog` 
            UNION ALL
            SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidPackageInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSensorInfo` 
            UNION ALL
            SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_{app_name}.AutoExportedAndroidSmsMms` 
            UNION ALL
            --SELECT TIMESTAMP_SECONDS(5 * DIV(unix_seconds(TIMESTAMP_SECONDS(time)), 5)) AS event_date, min(event_details) as event_details, min(source) as source
            SELECT TIMESTAMP_SECONDS(time) AS event_date, event_details, source
            from (
            select * from (
                SELECT time, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source  FROM `utility-ratio-120412.grr_{app_name}.new_jitmf_data`
            )
            WHERE (upper(event_details) like 'DELETE%' or upper(event_details) like 'INSERT%' or upper(event_details) like 'UPDATE%' or upper(event_details) like 'REPLACE%'  )
            AND (lower(event_details) like '% {app_table_name.lower()} %' OR lower(event_details) like '% {app_table_name.lower()}(%')    
            )
            --GROUP BY event_date
            UNION ALL
            {app_table_query}
            {all_telegram_tbl_union} 
        ) 
        WHERE event_date IS NOT NULL 
        AND event_date >= "{app_start_date}"
        AND event_date <= "{app_end_date}" 
        AND event_details like "%{keyword_content}%"
        {('AND (event_details like "DELETE FROM '+app_table_name+' %" OR event_details like "%deletetime%")' if delete_only else '')}
        )  """
    query_result = client.query(query) 
    # logging.error(query)
    results_time = set([x[0].strftime("%Y-%m-%d %H:%M:%S") for x in result_set])
    for row in query_result:
        if row not in result_set:
            if correlate_other_jitmf_events and row[2] == "new_jitmf_data":
                matches = re.finditer(msg_keyword, row[1], re.IGNORECASE)
                if list(matches):
                    if my_app_name == "skype":
                        if row[0].strftime("%Y-%m-%d %H:%M:%S") not in results_time: # We are only inserting 1 jitmf event per second to avoid duplication
                            result_set.append(row)
                            results_time.add(row[0].strftime("%Y-%m-%d %H:%M:%S"))
                        elif list(matches):
                            result_set = [rs for rs in result_set if rs[0].strftime("%Y-%m-%d %H:%M:%S") != row[0].strftime("%Y-%m-%d %H:%M:%S")]
                            result_set.append(row) # Updating this to the latest version
                    else:
                        result_set.append(row)
            else:
                result_set.append(row)
    
    return result_set
