-- Model generation based on model_feature_generation.sql

CREATE MODEL IF NOT EXISTS `utility-ratio-120412.grr_app_models.whatsapp_pca_60s`
OPTIONS
( MODEL_TYPE='PCA', PCA_EXPLAINED_VARIANCE_RATIO=0.8) AS
SELECT * EXCEPT(event_date)
FROM `utility-ratio-120412.grr_app_models.whatsapp_table_60s`;