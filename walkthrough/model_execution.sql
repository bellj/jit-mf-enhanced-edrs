SELECT * 
FROM ML.DETECT_ANOMALIES(
MODEL `utility-ratio-120412.grr_app_models.whatsapp_pcs_60s`,
STRUCT (0.3 AS contamination),
TABLE `utility-ratio-120412.grr_app_models.whatsapp_table_60s`)
where is_anomaly=true
ORDER BY event_date"""