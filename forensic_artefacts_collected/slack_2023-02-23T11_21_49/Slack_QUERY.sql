
SELECT max(ev) as ev, max(event_date) as Event_Date, ID AS Msg_ID, max(channel) as channel, max(event_details) as event_details from(
  select max(event_date) as ev, TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(max(event_details), r'[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}') as ID, REGEXP_EXTRACT(max(event_details), r'"channel":"[A-Z0-9]{11}"') as channel, max(event_details) as event_details
  from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_slack.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(message_json.ts as int64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",channel_id,"| in/out: ",channel_id,"| message: ",message_json.text) AS event_details, "slack_data" as source FROM `utility-ratio-120412.grr_slack.slack_data`  
    )
    WHERE event_date >= "2023-02-23 10:24:55 UTC" 
    AND event_date <= "2023-02-23 11:16:05 UTC" 
    AND event_details not like '%draft%'

    -- INSTALLATION
    -- AND event_details like '%com.example.demo%'

    -- PROPOGATION
    AND event_details like '%DHL%'
    GROUP BY event_date
)
where id is not null
and channel is not null
group by    ID

--- TO GET LOCAL IDS
select * from(
  select min(event_date), REGEXP_EXTRACT((event_details), r'SELECT ([a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})') as ID, min(event_details)
    from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_slack.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(message_json.ts as int64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",channel_id,"| in/out: ",channel_id,"| message: ",message_json.text) AS event_details, "slack_data" as source FROM `utility-ratio-120412.grr_slack.slack_data`  
    )
    WHERE event_date >= "2023-02-23 10:24:55 UTC" 
    AND event_date <= "2023-02-23 11:16:05 UTC" 
    AND event_details not like '%draft%'
    AND (event_details like '%client_msg_id":"020f12fe-8934-4fb0-a538-b5ea51fcd222%'
or event_details like '%client_msg_id":"26dad300-54d8-4e1d-ac71-a7fffc1eda70%'
or event_details like '%client_msg_id":"b41c2528-8429-426a-b150-3c2f773683b4%'
or event_details like '%client_msg_id":"caa3a668-252d-4716-8c6b-5484d2ab16cb%')
group by id)WHERE ID is not null

-- DELETION
select min(event_date), id, min(event_details) from (
    select event_date, REGEXP_EXTRACT((event_details), r'id[\s]*=[\s]*([a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12})') as ID, (event_details)
    from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_slack.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_slack.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(message_json.ts as int64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",channel_id,"| in/out: ",channel_id,"| message: ",message_json.text) AS event_details, "slack_data" as source FROM `utility-ratio-120412.grr_slack.slack_data`  
    )
    WHERE event_date >= "2023-02-23 10:24:55 UTC" 
    AND event_date <= "2023-02-23 11:16:05 UTC" 
    AND event_details not like '%draft%'
    AND event_details like '%DELETE%' 
)
where ((id LIKE '%dd2cea42-9224-444c-a6dd-a93e3b926c75%')
OR (id LIKE '%9750d112-8115-4b7b-b082-edca7b5f04a0%')
OR (id LIKE '%8a3597a0-ea38-40c0-b70f-d966a58ae9c5%')
OR (id LIKE '%c6a25b5e-0fb9-4bda-9685-831fc3d7369c%'))
GROUP BY id