SELECT  * from(
select TIMESTAMP_SECONDS(7 * DIV(unix_seconds(event_date), 7)) AS event_date, min(event_details) as event_details
from
(
    SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidCallLog` 
    UNION ALL
    SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidPackageInfo` 
    UNION ALL
    SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSensorInfo` 
    UNION ALL
    SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSmsMms` 
    UNION ALL
    SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_signal.new_jitmf_data`  
    UNION ALL
    SELECT TIMESTAMP_MILLIS(cast(receipt_timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",recipient_id,"| in/out: ",receipt_timestamp,"| message: ",body) AS event_details, "signal_data" as source FROM `utility-ratio-120412.grr_signal.signal_data`  
)
WHERE event_date >= "2023-03-05 17:36:31 UTC" 
AND event_date <= "2023-03-05 18:14:58" 

-- INSTALLATION
-- AND event_details like '%com.example.demo%'

-- PROPOGATION
    AND event_details like '%DHL: Your parcel is arriving, track here: https://flexisales.com/%'
    GROUP BY event_date    
)
WHERE event_details NOT LIKE '%drafts%'


--- TO GET IDS
-- SELECT  max(Event_Date), max(event_details) as Event_Details from(
    SELECT  max(event_date),  ID, MAX(event_details)  from(
    select TIMESTAMP_SECONDS(7 * DIV(unix_seconds(event_date), 7)) AS event_date, REGEXP_EXTRACT(min(event_details), r'^.*message._id = ([0-9]+)') as ID, min(event_details) as event_details
    from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_signal.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(receipt_timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",recipient_id,"| in/out: ",receipt_timestamp,"| message: ",body) AS event_details, "signal_data" as source FROM `utility-ratio-120412.grr_signal.signal_data`  
    )
    WHERE event_date >= "2023-03-05 17:36:31 UTC" 
    AND event_date <= "2023-03-05 18:14:58" 
 
    -- MID Search
    AND event_details like '%message._id =%'
    GROUP BY event_date    
)
WHERE event_details NOT LIKE '%drafts%'
and id is not null
and ( event_date = '2023-03-05 17:41:57 UTC' or event_date = '2023-03-05 17:43:21 UTC') -- AS PER INSERT RESULTS
group by id

---- for deletion (like WhatsApp)
select min(event_date), id, min(event_details) from (
    select event_date,  REGEXP_EXTRACT((event_details), r'^.*message_id = ([0-9]+)') as ID, event_details FROM
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_signal.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_signal.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(receipt_timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",_id,"| uid: ",recipient_id,"| in/out: ",receipt_timestamp,"| message: ",body) AS event_details, "signal_data" as source FROM `utility-ratio-120412.grr_signal.signal_data`  
    )
    WHERE event_date >= "2023-03-05 17:36:31 UTC" 
    AND event_date <= "2023-03-05 18:14:58" 
    AND event_details like '%DELETE%'
)
WHERE ID IN ('12','13')
group by id