SELECT max(event_date) as Event_Date, max(ID) AS Msg_ID, max(event_details) as Event_Details from(
    select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(max(event_details), r'^*[0-9]{5}') as ID, max(event_details) as event_details
    from
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, max(event_details) from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_plusmessenger.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_plusmessenger.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_plusmessenger.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_plusmessenger.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_plusmessenger.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_SECONDS(date) AS event_date, CONCAT("mid: ",mid,"| uid: ",uid,"| in/out: ",out,"| message: ",data) AS event_details, "telegram_data" as source FROM `utility-ratio-120412.grr_plusmessenger.plusmessenger_data`  
    )
    WHERE event_date >= "2023-03-15 12:34:25 UTC"
    AND event_date <= "2023-03-15 13:24:14 UTC"

    -- INSTALLATION
    -- AND event_details like '%com.example.demo%'

    -- PROPOGATION
    AND event_details like '%DHL: Your parcel is arriving, track here: https://%flex%'

    -- DELETE 
    -- AND ((event_details LIKE '%19090%' AND event_details LIKE '%DELETE%' )
    -- OR (event_details LIKE '%19032%' AND event_details LIKE '%DELETE%')
    -- OR (event_details LIKE '%19033%'AND event_details LIKE '%DELETE%')
    -- OR (event_details LIKE '%19034%' AND event_details LIKE '%DELETE%' )
    -- OR (event_details LIKE '%19035%' AND event_details LIKE '%DELETE%')
    -- OR (event_details LIKE '%19036%'AND event_details LIKE '%DELETE%'))

    GROUP BY event_date
)
group by ID