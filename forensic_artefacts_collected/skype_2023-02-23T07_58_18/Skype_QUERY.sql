-- use this
-- select * from(SELECT TIMESTAMP_MILLIS(nsp_data.createdTime) AS event_date, CONCAT("mid: ",sm.id ,"| uid: ",nsp_data.conversationId,"| in/out: ", nsp_data._isMyMessage ,"| message: ",sm.content) AS event_details,"skype_data" as source FROM `utility-ratio-120412.grr_skype.skype_data`  CROSS JOIN UNNEST(nsp_data._serverMessages) AS sm  WHERE nullif(ltrim(rtrim(sm.content)),'') is not null and (nsp_data.properties is null or nsp_data.properties.deletetime is null))
-- UNION ALL
-- select * from(SELECT TIMESTAMP_MILLIS(nsp_data.createdTime) AS event_date, CONCAT("mid: ",sm.id ,"| uid: ",nsp_data.conversationId,"| in/out: ", nsp_data._isMyMessage ,"| message: ",sm.content,"| deleted: ",nsp_data.properties.deletetime) AS event_details,"skype_data" as source FROM `utility-ratio-120412.grr_skype.skype_data`  CROSS JOIN UNNEST(nsp_data._serverMessages) AS sm  WHERE nullif(ltrim(rtrim(sm.content)),'') is not null and (nsp_data.properties is NOT null and  nsp_data.properties.deletetime is not null))
-- ORDER BY event_date

SELECT max(event_date) as Event_Date, max(ID) AS Msg_ID, max(event_details) as Event_Details from(
    select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(max(event_details), r'^*id["]*:[\s"]*([0-9]{13})["|\|]') as ID, max(event_details) as event_details 
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(min(event_details), r'^*[0-9]:live:\.cid\.[a-z0-9]{16}') as ID, min(event_details) as event_details
    from
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, max(event_details) from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_skype.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_skype.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(nsp_data.createdTime) AS event_date, CONCAT("mid: ",sm.id ,"| uid: ",nsp_data.conversationId,"| in/out: ", nsp_data._isMyMessage ,"| message: ",sm.content,"| deleted: ",nsp_data.properties.deletetime) AS event_details, "skype_data" as source FROM `utility-ratio-120412.grr_skype.skype_data`  CROSS JOIN UNNEST(nsp_data._serverMessages) AS sm  
    )
    WHERE event_date >= "2023-02-23 07:01:06 UTC" 
    AND event_date <= "2023-02-23 07:44:42 UTC" 
    and source like '%jitmf%'  -- without jitmf check

    -- INSTALLATION
    -- AND event_details like '%com.example.demo%'

    -- PROPOGATION
    -- AND event_details like '%DHL: Your parcel is arriving, track here: %https://flexisales.com/%'

    -- DELETE 
    AND ((event_details LIKE '%1677135904835%' AND upper(event_details) LIKE '%DELETE%' )
    OR (event_details LIKE '%1677136027986%' AND upper(event_details) LIKE '%DELETE%')
    OR (event_details LIKE '%1677136130994%'AND upper(event_details) LIKE '%DELETE%')
    OR (event_details LIKE '%1677136220922%' AND upper(event_details) LIKE '%DELETE%' ))

    GROUP BY event_date
)
where ID is not null
group by ID