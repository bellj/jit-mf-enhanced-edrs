SELECT max(event_date) as Event_Date, max(ID) AS Msg_ID, max(event_details) as Event_Details from(
    select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(max(event_details), r'[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}') as ID, max(event_details) as event_details
    from
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, max(event_details) from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_kik.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_kik.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",uid,"| uid: ",partner_jid,"| in/out: ",was_me,"| message: ",body) AS event_details, "kik_data" as source FROM `utility-ratio-120412.grr_kik.kik_data`    
    )
    WHERE event_date >= "2023-02-27 10:32:02 UTC" 
    AND event_date <= "2023-02-27 13:11:14 UTC" 
    -- INSTALLATION
    -- AND event_details like '%com.example.demo%'

    -- PROPOGATION
    -- AND event_details like '%DHL: Your parcel is arriving, track here: https://%flex%'

    -- DELETE 
    AND ((event_details LIKE '%cba8994d-0ea3-4f35-a412-21ad24a12405%' AND event_details LIKE '%DELETE%' )
    OR (event_details LIKE '%b903bc93-99bd-41b2-be1e-0dac4604eda4%' AND event_details LIKE '%DELETE%')
    OR (event_details LIKE '%5214897f-0aee-4bb3-8c49-a7f2026fc214%'AND event_details LIKE '%DELETE%'))

    GROUP BY event_date
)
group by ID