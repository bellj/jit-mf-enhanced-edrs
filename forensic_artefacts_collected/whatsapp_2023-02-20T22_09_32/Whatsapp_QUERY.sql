select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(min(event_details), r'^*,([1-9]),') as ID, min(event_details) as event_details
from
(
    SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidCallLog` 
    UNION ALL
    SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidPackageInfo` 
    UNION ALL
    SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSensorInfo` 
    UNION ALL
    SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSmsMms` 
    UNION ALL
    SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`  
    UNION ALL
    SELECT TIMESTAMP_MILLIS(cast(timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",_ID,"| uid: ",sender_jid_row_id,"| in/out: ",from_me,"| message: ",text_data) AS event_details, "whatsapp_data" as source FROM `utility-ratio-120412.grr_whatsapp.whatsapp_data`  
)
WHERE event_date >= "2023-02-20 21:14:00 UTC" 
AND event_date <= "2023-02-27 21:57:29" 

-- INSTALLATION
-- AND event_details like '%com.example.demo%'

-- PROPOGATION
-- AND event_details like '%DHL: Your parcel is arriving, track here: https://%flex%'

GROUP BY event_date

-- DELETION
select min(event_date), id, min(event_details) from (
    select event_date,  REGEXP_EXTRACT((event_details), r'^.*(chat_row_id = [1-9]+).*$') as ID, event_details
    from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_whatsapp.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_whatsapp.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp/1000 as INT64)) AS event_date, CONCAT("mid: ",_ID,"| uid: ",sender_jid_row_id,"| in/out: ",from_me,"| message: ",text_data) AS event_details, "whatsapp_data" as source FROM `utility-ratio-120412.grr_whatsapp.whatsapp_data`  
    )
    WHERE event_date >= "2023-02-20 21:14:00 UTC" 
    AND event_date <= "2023-02-27 21:57:29" 
    AND event_details like '%DELETE%' 
)
where ((id LIKE '%1%' )
    OR (id LIKE '%2%')
    OR (id LIKE '%4%')
    OR (id LIKE '%5%'))
GROUP BY id
