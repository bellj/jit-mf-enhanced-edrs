(window["webpackJsonp_act-55083-svip"]=window["webpackJsonp_act-55083-svip"]||[]).push([["chunk-50649e5e"],{"20ca":function(e,t,n){"use strict";n.d(t,"a",(function(){return a}));n("4795");var o=null;function a(e,t){clearTimeout(o),o=setTimeout((function(){e()}),t)}},"625b":function(e,t,n){"use strict";n("9874")},"7b2e":function(e,t,n){"use strict";var o=function(){var e=this,t=e.$createElement,n=e._self._c||t;return n("svg",{attrs:{height:"48",viewBox:"0 0 48 48",width:"48",xmlns:"http://www.w3.org/2000/svg"}},[n("g",{attrs:{fill:"none","fill-rule":"evenodd"}},[n("path",{attrs:{d:"m12.5857864 26.9857864-10.25077757-10.2507776c-.77873123-.7787312-.77873123-2.041304 0-2.8200352.0016608-.0016608.00332452-.0033187.00499117-.0049736.78505469-.7795261 2.05271354-.7772865 2.83500883.0050088l8.11788437 8.1178844c.3905243.3905243 1.0236893.3905243 1.4142136 0l20.0578844-20.05788437c.7787312-.77873123 2.041304-.77873123 2.8200352 0 .0016608.0016608.0033187.00332452.0049736.00499117.7795261.78505469.7772865 2.05271354-.0050088 2.83500883l-22.1707776 22.17077757c-.7810486.7810486-2.0473786.7810486-2.8284272 0z",fill:"currentColor","fill-rule":"nonzero",transform:"translate(4 10)"}}),n("path",{attrs:{d:"m0 0h48v48h-48z"}})])])},a=[],s=n("2877"),c={},i=Object(s["a"])(c,o,a,!1,null,null,null);t["a"]=i.exports},9874:function(e,t,n){},fda2:function(e,t,n){"use strict";n.r(t);var o=function(){var e=this,t=e.$createElement,n=e._self._c||t;return n("div",{staticClass:"recharge-discount"},[n("the-nav",{attrs:{"before-color":"#000","after-bg-color":"#fff",title:e.pageTitle},on:{"click-left":e.onBack}}),1===e.curStep?[n("div",{staticClass:"step-form-item",class:{"field-error":e.phoneError}},[n("div",{staticClass:"step-form-item__title1"},[e._v(e._s(e.$t("new002")))]),n("div",{staticClass:"step-form-item__content account-phone"},[n("div",{staticClass:"account__area",on:{click:e.showPhoneAreaDialog}},[n("div",{staticClass:"phone-code"},[e._v(e._s(e.phoneCC.value))]),n("div",{staticClass:"arrow"})]),n("div",{staticClass:"account__phone step-form-item__field"},[n("van-field",{ref:"phoneInput",attrs:{"label-class":"step-input",type:"number",maxlength:"20",clearable:""},on:{focus:function(t){return e.focusInput("phoneNumber")},blur:function(t){return e.blursInput("phoneNumber")},input:e.updatePhoneNumber},model:{value:e.phoneNumber,callback:function(t){e.phoneNumber=t},expression:"phoneNumber"}})],1),e.phoneError?n("div",{staticClass:"step-form-item__field-error"},[n("div",{staticClass:"step-form-item__field-error-text"},[n("SvgIconFail",{staticClass:"icon"}),n("div",{staticClass:"arrow"}),n("div",{staticClass:"text"},[e._v(e._s(e.$t("lang017")))])],1)]):e._e()]),n("div",{staticClass:"step-module__info-detail",class:{"main-field-gray":e.phoneFieldFocus||e.phoneError}},[e.phoneError?[n("span",{staticClass:"content-1"},[e._v(e._s(e.$t("new003")))]),n("span",{directives:[{name:"imo-lazy",rawName:"v-imo-lazy:avatar",value:"",expression:"''",arg:"avatar"}],key:"avatar-none",staticClass:"avatar"}),n("span",{staticClass:"content-2"},[e._v(e._s("None"))])]:e._e(),e.userProfile?[n("span",{staticClass:"content-1"},[e._v(e._s(e.$t("new003")))]),n("span",{directives:[{name:"imo-lazy",rawName:"v-imo-lazy:avatar",value:e.userIcon,expression:"userIcon",arg:"avatar"}],key:1,staticClass:"avatar"}),n("span",{staticClass:"content-2"},[e._v(e._s(e.userName))])]:e._e()],2),e.couponList?[e.couponList?n("div",{staticClass:"coupon",class:{"main-field-gray":e.phoneFieldFocus||e.phoneError}},[e.couponList.length?n("div",{staticClass:"coupon__title"},[e._v(" "+e._s(e.$t("new004"))+" ")]):e._e(),e._l(e.couponList,(function(t,o){return n("div",{key:o,staticClass:"coupon__card",on:{click:function(n){return e.handleSelectCoupon(t)}}},[n("div",{staticClass:"coupon__card-left"},[n("div",{staticClass:"coupon__card-left-bg"}),n("div",{staticClass:"circle-top"}),n("div",{staticClass:"circle-bottom"}),n("div",{staticClass:"coupon__card-left-info"},[n("div",{staticClass:"number"},[e._v(e._s("+"+(t.returnRate||0)))]),n("div",{staticClass:"percent"},[e._v("%")])])]),n("div",{staticClass:"coupon__card-info"},[n("div",{staticClass:"coupon__card-info--desc"},[e._v(" "+e._s(t.name)+" ")]),n("div",[n("span",{staticClass:"coupon__card-info--svip-icon"}),n("span",{staticClass:"coupon__card-info--svip-point"},[e._v(e._s(e.userProfile?t.exchangePoints:"-"))])])]),n("div",{staticClass:"coupon__btn",class:{select:t===e.selectCoupon}},[t===e.selectCoupon?n("SvgIconDone",{staticClass:"coupon__btn-tick"}):e._e()],1)])}))],2):e._e(),e.isBtnActive?n("div",{staticClass:"step-module__btn-wrapper"},[n("ConfirmBtn",{staticClass:"step-form-item__btn",attrs:{"btn-content":"Query","btn-width":660,"btn-height":80},on:{click:e.exchangeSvipPrivilege}}),n("div",{staticClass:"step-module__svip-point"},[n("div",{staticClass:"text"},[e._v(e._s(e.$t("lang007")))]),n("div",{staticClass:"icon"}),n("div",{staticClass:"point"},[e._v(e._s(e.curPoints))])])],1):e._e()]:e._e()],2)]:e._e(),2===e.curStep?[n("div",{staticClass:"step-module__success"},[n("div",{staticClass:"step-module__success-icon"}),n("div",{staticClass:"step-module__success-title"},[e._v(e._s(e.$t("lang027")))]),n("div",{staticClass:"step-module__success-desc"},[e._v(" "+e._s(e.$t("lang028"))+" ")]),n("ConfirmBtn",{attrs:{"btn-content":"OK","btn-height":72},on:{click:e.backDetailPage}})],1)]:e._e(),2===e.curStep?n("CustomService"):e._e(),n("dialog-phone-area-codes",{ref:"phoneCC",attrs:{"area-code":e.phoneCC},on:{"handle-select":e.handleSelectAreaCode}})],2)},a=[],s=n("c7eb"),c=n("1da1"),i=n("2909"),r=n("5530"),u=(n("be7f"),n("565f")),l=(n("b0c0"),n("7db0"),n("d3b7"),n("c975"),n("fb6a"),n("4795"),n("99af"),n("d81d"),n("47c1")),p=n("2146"),d=n("947d"),h=n("05cc"),v=n("dde5"),f=n("6442"),_=n("5880"),C=n("b09a"),b=n("7b2e"),m=n("d8a9"),g=n("20ca"),w=n("dfd1");p["a"].use(u["a"]);var P={name:"RechargeDiscount",components:{DialogPhoneAreaCodes:function(e){return n.e("chunk-12f73688").then(function(){var t=[n("0dd6")];e.apply(null,t)}.bind(this)).catch(n.oe)},ConfirmBtn:d["a"],TheNav:h["a"],CustomService:f["a"],SvgIconDone:b["a"],SvgIconFail:w["a"]},data:function(){return{curStep:1,phoneAreaList:l["a"],phoneNumber:"",phoneCode:"",phoneCC:{value:"",code:""},phoneError:!1,phoneFieldFocus:!1,userProfile:null,canUnblock:!1,couponList:[],selectCoupon:null,userIcon:""}},computed:Object(r["a"])(Object(r["a"])({},Object(_["mapGetters"])(["curDetailPrivilege","curLevel","curPoints","userPhone","userCc"])),{},{pageTitle:function(){var e;return(null===(e=this.curDetailPrivilege)||void 0===e?void 0:e.name)||""},isBtnActive:function(){return!!this.selectCoupon&&!this.phoneFieldFocus},userName:function(){var e;return(null===(e=this.userProfile)||void 0===e?void 0:e.name)||""},privilegeType:function(){return"SellerRechargeDiscount"===this.$route.name?"diamonds_seller_recharge_discount":"recharge_discount"},rechargeRequireLevel:function(){var e=this,t=this.privilegeInfos.find((function(t,n){return(null===t||void 0===t?void 0:t.svipPrivilegeType)===("SellerRechargeDiscount"===e.$route.name?"diamonds_seller_recharge_discount":"recharge_discount")}))||[];return Math.min.apply(Math,Object(i["a"])(null===t||void 0===t?void 0:t.svipLevels))}}),mounted:function(){var e=this;return Object(c["a"])(Object(s["a"])().mark((function t(){return Object(s["a"])().wrap((function(t){while(1)switch(t.prev=t.next){case 0:e.userPhone&&e.handlePhoneFill(e.userPhone),Object(C["m"])(!0,e.onBack);case 2:case"end":return t.stop()}}),t)})))()},watch:{phoneNumber:function(e){if(!e)return this.phoneError=!1,this.selectCoupon=null,this.userProfile=null,void(this.couponList=[]);this.userIcon="",this.selectCoupon=null,Object(g["a"])(this.getCouponInfo,500)},phoneCC:function(e){this.phoneError=!1,this.userIcon="",this.selectCoupon=null,Object(g["a"])(this.getCouponInfo,500)},userPhone:function(e){this.handlePhoneFill(e)}},methods:{initAreaCode:function(){var e=this;return Object(c["a"])(Object(s["a"])().mark((function t(){var n,o;return Object(s["a"])().wrap((function(t){while(1)switch(t.prev=t.next){case 0:try{n={value:"+86",code:"CN"},e.userCc?(o=e.phoneAreaList.find((function(t){return t.code===e.userCc})),o||(o=n),e.phoneCC=o):e.phoneCC=n}catch(a){console.log(a)}case 1:case"end":return t.stop()}}),t)})))()},handlePhoneFill:function(e){var t=this;this.initAreaCode().then((function(){if(e){var n=e.indexOf(t.phoneCC.value);if(-1!==n){var o=t.phoneCC.value.length,a=e;C["c"].track("recharge-discount","auto__fill__phone:".concat(a.slice(o))),t.updatePhoneNumber(a.slice(o))}else C["c"].track("recharge-discount","auto__fill__fail"),t.updatePhoneNumber("")}}))},updatePhoneNumber:function(e){this.phoneNumber=e},handleSelectAreaCode:function(e){this.showArea=!1,this.phoneCC={value:e.value,code:e.code}},focusInput:function(e){this.phoneFieldFocus=!0},blursInput:function(e){var t=this;this.$nextTick((function(){setTimeout((function(){t.phoneFieldFocus=!1}),100)}))},showPhoneAreaDialog:function(){this.showArea=!0,this.$refs.phoneCC.open(this.phoneCC)},getCouponInfo:function(){var e=this;return Object(c["a"])(Object(s["a"])().mark((function t(){var n,o,a,c,i,u;return Object(s["a"])().wrap((function(t){while(1)switch(t.prev=t.next){case 0:if(e.phoneNumber){t.next=2;break}return t.abrupt("return");case 2:return C["c"].track("recharge-discount","before__getCouponInfo__".concat(e.phoneNumber)),t.next=5,v["i"]({phone:"".concat(e.phoneCC.value).concat(e.phoneNumber),privilegeType:e.privilegeType,language:e.phoneCC.code});case 5:if(i=t.sent,0===i.code){t.next=10;break}return e.phoneError=!0,e.userProfile=null,t.abrupt("return");case 10:C["c"].track("recharge-discount","getCouponInfo__success"),e.phoneError=!1,e.userProfile=null===(n=i.data)||void 0===n?void 0:n.profile,e.userIcon=(null===(o=e.userProfile)||void 0===o?void 0:o.icon)||"",u=(null===(a=i.data)||void 0===a||null===(c=a.rewardConfigs)||void 0===c?void 0:c.map((function(e,t){var n=(null===e||void 0===e?void 0:e.reward)||{};return Object(r["a"])(Object(r["a"])({},e),n)})))||[],e.couponList=u,e.selectCoupon=e.couponList[0];case 17:case"end":return t.stop()}}),t)})))()},exchangeSvipPrivilege:function(){var e=this;return Object(c["a"])(Object(s["a"])().mark((function t(){var n,o,a,c,i;return Object(s["a"])().wrap((function(t){while(1)switch(t.prev=t.next){case 0:if(a=(null===(n=e.selectCoupon)||void 0===n?void 0:n.detailId)||"",C["c"].track("recharge-discount","before__exchangeCoupon__isBtnActive:".concat(e.isBtnActive,"__couponId:").concat(a)),window.statPoint({action:"208",level:e.curLevel}),e.isBtnActive){t.next=5;break}return t.abrupt("return");case 5:return t.next=7,v["b"]({phone:"".concat(e.phoneCC.value).concat(e.phoneNumber),privilegeType:e.privilegeType,detailId:a});case 7:if(c=t.sent,0===c.code){t.next=15;break}return 73001===c.code&&m["a"].toast(e.$t("new015")),73002===c.code&&m["a"].toast(e.$t("lang067",{n1:"SVIP".concat(e.rechargeRequireLevel)})),73003===c.code&&m["a"].toast(e.$t("lang069")),73004===c.code&&(m["a"].toast(e.$t("lang017")),e.phoneError=!0,e.phoneFieldFocus=!1),60006===c.code&&m["a"].toast(e.$t("lang070")),t.abrupt("return");case 15:window.statPoint({action:"209",level:e.curLevel,coupon_id:a}),C["c"].track("recharge-discount","exchangeCoupon__success"),i=(null===(o=e.selectCoupon)||void 0===o?void 0:o.exchangePoints)||0,e.$store.commit("curPoints",e.curPoints-i),e.curStep=2;case 20:case"end":return t.stop()}}),t)})))()},onBack:function(){2===this.curStep?this.curStep=1:this.backDetailPage()},handleSelectCoupon:function(e){this.phoneError||(this.selectCoupon===e&&(this.selectCoupon=null),this.selectCoupon=e)},backDetailPage:function(){history.back(),Object(C["m"])(!1)}}},k=P,x=(n("625b"),n("2877")),O=Object(x["a"])(k,o,a,!1,null,"4003e74e",null);t["default"]=O.exports}}]);
//# sourceMappingURL=https://frontmon-sysop.bigo.sg/map/imo.im/act-55083-svip/sourcemaps/chunk-50649e5e.e2fdc2d8.js.map