SELECT max(event_date) as Event_Date, max(ID) AS Msg_ID, max(event_details) as Event_Details from(
    select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(max(event_details), r'^*[0-9]+') as ID, max(event_details) as event_details
    from
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, max(event_details) from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_imo.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp/1000000 as INT64)) AS event_date, CONCAT("mid: ",msg_id,"| uid: ",group_msg_id,"| in/out: ",message_state,"| message: ",last_message) AS event_details, "imo_data" as source FROM `utility-ratio-120412.grr_imo.imo_data`  
    )
    WHERE event_date >= "2023-02-27 16:04:04 UTC" 
    AND event_date <= "2023-02-27 16:13:16 UTC" 

    -- INSTALLATION
    -- AND event_details like '%com.example.demo%'

    -- PROPOGATION
    -- AND event_details like '%DHL: Your parcel is arriving, track here: https://%flex%'

    -- DELETE 
    AND ((event_details LIKE '%1109846348587321%' AND event_details LIKE '%DELETE%' )
    OR (event_details LIKE '%1106220383085401%' AND event_details LIKE '%DELETE%')
    OR (event_details LIKE '%1102962116618439%'AND event_details LIKE '%DELETE%'))

    GROUP BY event_date
)
group by ID



-------------------- INV. PROCESS METHODOLOGY WITH WHATSAPP APPROACH FOR IMO

-- SELECT max(event_date) as Event_Date, max(ID) AS Msg_ID, max(event_details) as Event_Details from(
    SELECT max(event_date), ID, max(event_details) FROM 
    (
        select event_date,  REGEXP_EXTRACT((event_details), r'^.*buid=([0-9]{16})') as ID,  event_details
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(max(event_details), r'^*[0-9]+') as ID, max(event_details) as event_details
    from
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, max(event_details) from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_imo.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_imo.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(cast(timestamp/1000000 as INT64)) AS event_date, CONCAT("mid: ",msg_id,"| uid: ",group_msg_id,"| in/out: ",message_state,"| message: ",last_message) AS event_details, "imo_data" as source FROM `utility-ratio-120412.grr_imo.imo_data`  
    )
    WHERE event_date >= "2023-02-27 16:04:04 UTC" 
    AND event_date <= "2023-02-27 16:13:16 UTC" 

    -- INSTALLATION
    -- AND event_details like '%com.example.demo%'

    -- PROPOGATION
    -- AND event_details like '%DHL: Your parcel is arriving, track here: https://%flex%'

    -- DELETE 
    AND ((event_details like '%DELETE%'))
    AND ((event_details LIKE '%1109846348587321%' AND event_details LIKE '%DELETE%' )
    OR (event_details LIKE '%1106220383085401%' AND event_details LIKE '%DELETE%')
    OR (event_details LIKE '%1102962116618439%'AND event_details LIKE '%DELETE%'))

--     GROUP BY event_date
)
WHERE ID is not null
-- AND ID IN ('1109846348587321',    '1106220383085401',    '1102962116618439')
group by ID