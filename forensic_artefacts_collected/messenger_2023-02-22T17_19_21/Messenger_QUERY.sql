SELECT max(event_date) as Event_Date, max(ID) AS Msg_ID, max(event_details) as Event_Details from(
    select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, REGEXP_EXTRACT(min(event_details), r'mid\.\$[a-z0-9A-Z]+') as ID, min(event_details) as event_details
    from
    -- select TIMESTAMP_SECONDS(5 * DIV(unix_seconds(event_date), 5)) AS event_date, max(event_details) from
    (
        SELECT date AS event_date, CONCAT(phoneNumber, "|", contactName, "|", duration) AS event_details, "AutoExportedAndroidCallLog" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidCallLog` 
        UNION ALL
        SELECT lastUpdateTime AS event_date, CONCAT(packageName,"|",firstInstallTime,"|",lastUpdateTime) AS event_details, "AutoExportedAndroidPackageInfo" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidPackageInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(name,"|",type,"|",vendor) AS event_details, "AutoExportedAndroidSensorInfo" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidSensorInfo` 
        UNION ALL
        SELECT metadata.timestamp AS event_date, CONCAT(dateSent,"|",dateReceived) AS event_details, "AutoExportedAndroidSmsMms" as source FROM `utility-ratio-120412.grr_messenger.AutoExportedAndroidSmsMms` 
        UNION ALL
        SELECT TIMESTAMP_SECONDS(time) AS event_date, CONCAT(object.Query_Executed,"|",event) AS event_details, "new_jitmf_data" as source FROM `utility-ratio-120412.grr_messenger.new_jitmf_data`  
        UNION ALL
        SELECT TIMESTAMP_MILLIS(timestamp_ms) AS event_date, CONCAT("mid: ",message_id,"| uid: ",sender_id,"| in/out: ",sender_id,"| message: ",text) AS event_details, "messenger_data" as source FROM `utility-ratio-120412.grr_messenger.messenger_data`  
    )
    WHERE event_date >= "2023-02-22 16:22:44 UTC" 
    AND event_date <= "2023-02-22 17:01:58 UTC" 

    -- INSTALLATION
    -- AND event_details like '%com.example.demo%'

    -- PROPOGATION
    -- AND event_details like '%DHL: Your parcel is arriving, track here: https://%flex%'

    -- DELETE 
    AND ((event_details LIKE '%mid.$cAAAB8WQVzuGMqzevw2GefJsmgHtz%' AND event_details LIKE '%DELETE%' )
    OR (event_details LIKE '%mid.$cAAAB8WQVzuGMqzihDWGefNd8jlLv%' AND event_details LIKE '%DELETE%')
    OR (event_details LIKE '%mid.$cAABbAhwLuiKMqzl0HmGefQw4fQx0%'AND event_details LIKE '%DELETE%'))

    GROUP BY event_date
)
group by ID