This work was published as part of a journal paper: 
**J Bellizzi, E Losiouk, M Conti, C Colombo, M Vella - Journal of Cybersecurity and Privacy, 2023
[VEDRANDO: A Novel Way to Reveal Stealthy Attack Steps on Android through Memory Forensics](https://www.mdpi.com/2624-800X/3/3/19)**

# JIT-MF-enhanced EDRS Repository organisation

./forensic_artefacts_collected/* -> artefacts collected by JIT-MF-enhanced EDR. The folder contains a sub-folder for each app \
./frida-material/* -> Frida gadget version used and config \
./im_hijack_simulation_scripts/* -> Android View Client scripts used to simulate messaging hijack attacks on each app \
./models/* -> Anomaly Detection Models generated \
./walkthrough/* -> walktrhough of experiment including set up scripts \
sqlite-jitmf-driver.js -> JIT-MF infrastructure-based Driver used to collect evidence from memory
