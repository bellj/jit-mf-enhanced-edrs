import re
import sys
import time
import os
import string
from datetime import datetime

from com.dtmilano.android.viewclient import ViewClient

MAX_WAIT=10
_s = 20
_s2 = 1

def send_message(v):
    # sending message
    v.touch()
    vc.sleep(_s2)
    dump_handler()    
    view = vc.findViewsWithAttribute('class', 'android.widget.EditText')[0]
    view.touch()
    message_text="DHL: Your parcel is arriving, track here: https://flexisales.com/dhl?eep7j88cc5z3"
    view.setText(message_text)
    dump_handler()
    vc.sleep(_s2)
    vc.findViewByIdOrRaise("org.thoughtcrime.securesms:id/send_button").touch()
    print("SIGNAL | Normal traffic | Sent message text="+message_text+". Current time ... ----> "+ str(datetime.now()))
    dump_handler()
    vc.sleep(_s2)

def delete_last_message():
    list_l=vc.findViewsWithAttribute('resource-id', 'org.thoughtcrime.securesms:id/conversation_item_body')
    last_message=list_l[-1]

    (x,y)=last_message.getXY()
    y+=10
    last_message.device.drag((x,y), (x,y), 3000, 1)
    vc.sleep(_s2)
    dump_handler()
    # delete
    (x,y) = vc.findViewByIdOrRaise("org.thoughtcrime.securesms:id/attach_button").getXY()
    y-=10
    y-=5
    device.touch(x,y,0)

    dump_handler()
    vc.findViewWithTextOrRaise(u'Delete for me').touch()

    print("SIGNAL | Deleted message with text="+last_message.getText()+". Current time ... ----> "+ str(datetime.now()))

    
def send_and_delete_message(v):
    send_message(v)
    delete_last_message()
    dump_handler()
    vc.sleep(_s2)
    (x,y)=vc.findViewByIdOrRaise("org.thoughtcrime.securesms:id/contact_photo_image").getXY()
    x-=10
    device.touch(x,y,0) #back
    vc.sleep(_s2)

def get_contacts():
    views=[]
    views = vc.findViewsWithAttribute('resource-id', 'org.thoughtcrime.securesms:id/conversation_list_item_name')
    
    return views

def dump_handler():
    number_of_retries = 10
    sleep_time = 5
    counter = 0
    e=None
    while (counter < number_of_retries):
        try:
            vc.dump(window=-1)
            return
        except Exception as exc:
            e=exc
            time.sleep(sleep_time)
            counter += 1
    print("[[ FAILED TO EXECUTE VC.DUMP LATEST EXCEPTION ]]" + str(e))

device = sys.argv[1]
sys.argv = [sys.argv[0]] #delete args

kwargs1 = {'verbose': False, 'ignoresecuredevice': False, 'ignoreversioncheck': False}
device, serialno = ViewClient.connectToDeviceOrExit(serialno=device,**kwargs1)

device.startActivity('com.example.trustedcontainervirtualapp/com.example.trustedcontainervirtualapp.MainActivity')
kwargs2 = {'forceviewserveruse': False, 'startviewserver': True, 'autodump': False, 'ignoreuiautomatorkilled': True, 'compresseddump': True, 'useuiautomatorhelper': False, 'debug': {}}
vc = ViewClient(device, serialno, **kwargs2)

vc.sleep(_s)
dump_handler()
views=get_contacts()

for v in views:
    send_and_delete_message(v)

device.shell('input keyevent KEYCODE_BACK')
vc.sleep(_s2)
device.shell('input keyevent KEYCODE_HOME')
